from keras.preprocessing.sequence import pad_sequences

import numpy as np
import pandas as pd
import nltk
import pickle
# imports for lexicon-based
from nltk.sentiment.util import mark_negation
from nltk.corpus import stopwords
from nltk.corpus import wordnet
from nltk.stem import WordNetLemmatizer
from nltk.corpus import sentiwordnet as swn
import time

import warnings

def load(file_name):
    fileObject = open(file_name,'rb')
    item = pickle.load(fileObject)
    return item

def pre_process(review_text):
    ## Remove numbers,punctuations and whitespaces from text
    review_text = review_text.str.replace('\d+','')
    review_text = review_text.str.replace('[^\w\s]','')
    review_text = review_text.str.strip()

    ## Convert to lower Case
    review_text = review_text.str.lower()

    ## Perform Tokenization
    review_text_2 = review_text.apply(nltk.word_tokenize)

    ## Remove stop words from the tokenized list
    stop_words = nltk.corpus.stopwords.words('english') + ["xs","xl","l","s","m","xxs","xxl"]
    review_text_2 = review_text_2.apply(lambda x: [item for item in x if item not in stop_words])

    ## Perform lemmatization, convert tokens back to root word
    lemmatizer = nltk.stem.WordNetLemmatizer()
    def lemmatize_text(text):
        return [lemmatizer.lemmatize(w) for w in text]
    review_text_2 = review_text_2.apply(lemmatize_text)

    ## Get the length of the review with most words
    text_lengths = review_text_2.apply(lambda x: len(x))
    max_review_length = len(review_text_2[text_lengths.idxmax()])

    ## Converts array back to string, use for vectorizers/tokenizers
    review_text_2= review_text_2.apply(lambda x: " ".join(x))

    return max_review_length, review_text_2

def lstm_predictor(type,text):
    lstm_tokenizer = load("lstm_tokenizer")
    token_text = lstm_tokenizer.texts_to_sequences(text)
    pad_text = pad_sequences(token_text,maxlen=58,padding="post")
    if type == 'glove_cnn':
        model = load("lstm_glove_cnn_bin_model")
        model_rate = load("lstm_glove_cnn_cat_model")
    elif type == 'glove':
        model = load("lstm_glove_bin_model")
        model_rate = load("lstm_glove_cat_model")
    elif type == 'naive' :
        model = load("lstm_bin_model")
        model_rate = load("lstm_cat_model")
    prediction_model = model.predict(pad_text)
    prediction_rate = model_rate.predict(pad_text)
    return int(prediction_model[0][0].round()),prediction_rate.argmax(axis=1)[0]

def svm_predictor(text):
    svm_vectorizer = load("svm_vectorizer")
    svm_model = load("svm_bin_model")
    model_rate = load("svm_cat_model")
    vectorized_text = svm_vectorizer.transform(text)
    prediction_model = svm_model.predict(vectorized_text)
    predict_rate = model_rate.predict(vectorized_text)
    return(prediction_model[0],predict_rate[0])

'''
functions for lexicon-based
'''

def lexicon_predictor(text):
    startTime = time.time()
    text = text.lower() # convert all review text to lower case
    text = text.replace('\d+','') # remove numbers
    text = text.strip() # remove leading and ending white spaces
    text = nltk.word_tokenize(text) # tokenization
    text = nltk.pos_tag(text) # apply POS-tags
    text = markNegation(text)
    text = removeStopwords(text)
    text = lemmatizeTokens(text)
    preprocessDone = time.time()
    preprocessTime = preprocessDone - startTime # time taken to pre-process
    score = calculateTextScore_NH(text)
    sentiment = normalizeBinary(score)
    predictTime = time.time() - preprocessDone # time taken to calculate score and predict sentiment
    runtime = time.time() - startTime # total time taken

    return score, sentiment, runtime, preprocessTime, predictTime

def markNegation(pos_tagged_tokens):
    '''
    This function takes as input an array with POS-tagged tokens (tuples of size 2 containing the word, POS tag)
    Return: A new array of tokens -- tuples of size 3 containing the word, POS tag, and a boolean (true means negated)
    '''
    strTokens = [] # array of tokens with POS tags removed (needed for nltk.sentiment.util.mark_negation function)
    for token in pos_tagged_tokens:
        strTokens.append(token[0])

    strTokens = nltk.sentiment.util.mark_negation(strTokens)

    output = []
    for i in range(len(strTokens)):
        if "_NEG" in strTokens[i]:
            output.append((pos_tagged_tokens[i][0], pos_tagged_tokens[i][1], True))
        else:
            output.append((pos_tagged_tokens[i][0], pos_tagged_tokens[i][1], False))

    return output

def removeStopwords(pos_tagged_tokens):
    stop_words = set(stopwords.words('english'))
    return [token for token in pos_tagged_tokens if not token[0] in stop_words]

def penn_to_wn(tag):
    """
    Convert between the PennTreebank tags to simple Wordnet tags
    """
    if tag.startswith('J'):
        return wordnet.ADJ
    elif tag.startswith('N'):
        return wordnet.NOUN
    elif tag.startswith('R'):
        return wordnet.ADV
    elif tag.startswith('V'):
        return wordnet.VERB
    else:
        return None


def lemmatizeTokens(pos_tagged_tokens):
    """
    Lemmatize an array of tuples (tokens and their corresponding PennTreebank POS tag)
    The POS tags will be converted to Wordnet format
    """
    lemmatizer = WordNetLemmatizer()

    result = []
    for token in pos_tagged_tokens:
        wnPos = penn_to_wn(token[1])
        if wnPos is not None:
            result.append((lemmatizer.lemmatize(token[0], wnPos), wnPos, token[2]))
        else:
            result.append((token[0], wnPos, token[2]))

    return result

def calculateWordScore(word, pos):
    """
    Calculate the sentiment orientation of a word (given its pos tag)
    by taking average of all its senses' net score (pos_score - neg_score)
    """
    result = 0

    if pos != None:
        synsets = list(swn.senti_synsets(word, pos))

        for synset in synsets:
            result += synset.pos_score()
            result -= synset.neg_score()

        if (len(synsets) != 0):
            result /= len(synsets)

    return result

def calculateTextScore_NH(pos_tagged_tokens):
    """
    This is the version of calculateTextScore with Negation Handling.
    Take an array of tuples (tokens, their pos tags and negation boolean) as input and
    calculate the sentiment orientation of the text
    """
    sum = 0
    for token in pos_tagged_tokens:
        if token[2] == True:
            sum -= calculateWordScore(token[0], token[1])
        else:
            sum += calculateWordScore(token[0], token[1])

    return sum

def normalizeBinary(score):
    """
    Normalizes a sentiment score to either 0 (negative) or 1 (positive)
    """
    return 0 if score < 0 else 1


if __name__ == "__main__":
    warnings.simplefilter('ignore')
    while True:
        new_input = input("\nEnter Review Text or type 'quit' to quit: ")
        if new_input == "quit":
            quit()
        else:
            Review_Text = pd.Series([new_input])
            #Review_Text = pd.Series(["I liked that shirt but unfortunately there are torn spots all over"])
            #Review_Text = pd.Series(["I hate that shirt as there are torn spots all over"])
            #Review_Text= pd.Series(["WOW such a great uniform this is a must buy i would love to see another one of these series to appear again"])
            #Review_Text= pd.Series(["Dissapointed with the result this shirt is beyond bad"])
            empty,text= pre_process(Review_Text)
            result=['negative','positive']
            senti,rating = lstm_predictor("glove",text)
            print()
            print("Input Text: ",new_input)
            print()
            print("============================= LSTM ==============================")
            print("LSTM predicts:",result[senti],", Rating: ",rating)
            senti,rating = svm_predictor(text)
            print()
            print("============================= SVM ==============================")
            print("SVM predicts:",result[senti],", Rating: ",rating)

            # lexicon-based (negation handling version used)
            score, sentiment, runtime, preprocessTime, predictTime = lexicon_predictor(new_input)
            print()
            print("===================== Lexicon-based approach =====================")
            print("Overall Sentiment Score:", score)
            print("Predicted Sentiment:", "positive" if sentiment==1 else "negative")
            print("pre-processing runtime:", preprocessTime)
            print("classification runtime:", predictTime)
            print("total runtime:", runtime)
